
(
var make = { arg name, nb, detune, noise, phase, vols;

	var harmos =
	Array.rand(nb, 0.001, detune) +++
	Array.rand(nb, 0.001, noise) +++
	Array.rand(nb, 0.001, phase) +++
	(Array.rand(nb, 0.001, vols).collect({arg x, i; if (i % 2 == 1, 1, 1)}));

	SynthDef(name,  { arg freq, gate = 1, amp;
		var sig =
		harmos.collect({arg d, i;
			SinOsc.ar(
				(freq * (i + 1 + d[0])) + PinkNoise.kr(d[1]),
				d[2], d[3] * Env.adsr.ar(2, gate)
			)
		}).reduce('+') / 16;
		Out.ar([0, 1], sig * Env.adsr.ar(2, gate) * 1);
	}).add;
};

s.boot;
make.(\io, 3, 10, 10, 1, 1);
make.(\io2, 2, 0.1, 1, 1, 1);
)

(
Prout({
	var m = SimpleMIDIFile.read("C:/Users/simon/Downloads/fond-memories.mid");

	m.noteEvents(nil, 1).pairsDo({|a, b|
		var note = a[4];
		var amp = a[5];
		var dur = (b[1] - a[1]) / 1000;

		(\instrument: \io, midinote: note, dur: dur).play;
		(dur).yield;
	})
}).play;
)