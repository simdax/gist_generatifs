MyMain
{
	*initClass {
		StartUp.add({
			"starting engine".postln;
			MyServer();
			MyBuffers();
			Music();
		});
	}
}

MyBuffers
{
	classvar <buffers;
	// classvar dir = "C:\Users\simon\Documents\Supercollider\vg_engine\Assets";
	classvar dir = "D:/godot/godot-make-pro-2d-games";
	// thisProcess.nowExecutingPath.dirname +/+ "Assets"

	*new {
		Buffer.freeAll;
		buffers = ();
		PathName(dir.standardizePath).filesDo{|file|
			if ((file.extension == "wav") || (file.extension == "ogg"))
			{
				var key = file.fileNameWithoutExtension;
				buffers[key.asSymbol] = SoundFile.openRead(file.fullPath);
			}
		};
		"buffers set:".postln;
		buffers.postln;
	}
}

MyServer
{
	*new{
		var s = Server.local();

		s.waitForBoot({

			SynthDef(\testbleep,{ arg freq = 400;
				Out.ar(0,
					Pan2.ar(
						Line.kr(1,0,0.1,doneAction:2) * SinOsc.ar(freq)*0.1,0.0))
			}).add;


			SynthDef(\sfx, { arg buf;
				var sig = PlayBuf.ar(
					2, buf
				);
				Out.ar(0, sig);
			}).add;

			OSCdef(\pattern, { |m|
				var pattern = m[1].postln;
				var trigger = m[2].postln;
				if (trigger, {
					Pdef(pattern).play;
				}, {
					Pdef(pattern).stop;
				});
			}, \pattern);

			OSCdef(\synth, { arg args;
				Synth(args[1].asSymbol,
					[\freq,
						Scale.new(#[2,4,7,9,11])
						.degreeToFreq(
						args[2], 60.midicps, 1
				)]);
			}, \synth);

			OSCdef(\test, { arg args;
				args[1..].collect{arg a, i, b; if(i % 2 == 0){a.asSymbol}{a}}
				.asEvent.play
			}, \event);

			OSCdef(\sfx, { arg args;
				var buffer = MyBuffers.buffers[args[1]];
				play { PlayBuf.ar(buffer.numChannels, buffer.asBuffer) };
			}, \sfx);

			OSCdef(\close, {
				Pdef.all.do(_.stop)
			}, \close)
		})
	}
}


Music
{
	*new {
		//
		var base = Pbind(
			\degree, Pxrand([0, 1, 2, 4, 5], inf)
		).asStream;
		var mels = [
			[[degree: Pseq([-2, 2]), dur: 1/2]],
			[[degree: Pseq([\s, 1, 0]), dur: 1/4], [degree: Pseq([0]), \dur: 1/4]],
			// [[degree: Pseq([0, 1, 0]), dur: 1/4], [degree: Pseq([0]), \dur: 1/4]]
		];

		var evs = [(), ()];
		mels = mels.collect({ arg mel;
			Plazy{
				Pseq(mel.collect({arg ev, i;
					Pmono(\default, *(ev ++ [mtranspose: evs[i].degree]) )
				}))
			}
		});
		evs[0] = base.next(());

		Pdef(\base,
			Pbind(
				\amp, 0,
				\finish, p{
					loop {
						evs[1] = base.next(());
						true.yield;
						evs[0] = evs[1];
					}
				}
			)
		);
		Pdef(\mel,
			Ppar([
				Pdict(mels, Prand([0, 1], inf)),
				Pbind(
					\dur, 1,
					\octave, 4,
					\degree, Pfunc { evs[0].degree }
				),
			])
		)
	}
}


